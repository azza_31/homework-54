import React from "react";
import "./App.css"
import Field from "./containers/Field/Field";

const App = () => {
    return (
        <div>
            <Field/>
        </div>
    )
}

export default App;
