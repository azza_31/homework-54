import React, {useState} from "react";
import "./Field.css";
import Cell from "../../components/Cell/Cell";

const Field = () => {
    const [cells, setCells] = useState([]);
    const [counter, setCounter] = useState(0);

    const getStart = () => {
        const array = [];
        for (let i = 0; i < 36; i++) {
            const newCell = {
                hasRing: false,
                isOpen: false,
                id: i
            };

            array.push(newCell);
        }

        const randIndex = Math.floor(Math.random() * array.length);
        const randCell = array[randIndex];
        randCell.hasRing = true;

        setCells(array);
        setCounter(0);
    };

    const onCellClick = (targetId) => {
        const cellsCopy = [...cells];

        const index = cellsCopy.findIndex(el => {
            return el.id === targetId;
        });

        if (cellsCopy[index].isOpen === false) {
            setCounter(counter + 1);
        }

        cellsCopy[index].isOpen = true;

        setCells(cellsCopy);
    };

    const elements = [];

    for (let i = 0; i < cells.length; i++) {
        const cell = cells[i];

        const component = (
            <Cell
                key={cell.id}
                ring={cell.hasRing}
                open={cell.isOpen}
                onClick={() => onCellClick(cell.id)}
            />
        );

        elements.push(component);
    }

    return (
        <React.Fragment>
            <button className='btn-click' onClick={getStart}>Start Game</button>
            <div className="Field">
                {elements}
            </div>
            <div className="Counter">
                {counter + " : tries"}
            </div>
        </React.Fragment>

    );
};

export default Field;